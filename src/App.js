import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Lobby from './pages/Lobby';
import GameRoom from './pages/GameRoom';
import ROUTES from './pages/routes';

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path={ROUTES.HOME} component={Lobby} />
        <Route exact path={ROUTES.GAME} component={GameRoom} />
      </Switch>
    </div>
  );
}

export default App;
