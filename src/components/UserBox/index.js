import React from 'react';
import './UserBox.scss';
import PlaceholderIcon from '../../assets/icons/main-ic.svg';
import BalanceIcon from '../../assets/icons/balance-ic.svg';
import SpinsIcon from '../../assets/icons/spins-ic.svg';

const UserBox = ({ setModal }) => {
  return (
    <div className="UserBox">
      <button
        className="history-button"
        onClick={() =>
          setModal({
            isOpen: true,
            modalContent: 'history'
          })
        }
      >
        ისტორია
      </button>
      <div className="user-image-wrapper">
        <img
          src={PlaceholderIcon}
          alt="placeholder"
          className="image-placeholder"
        />
      </div>
      <div className="user-title">Muscul Bones</div>
      <div className="user-data -balance">
        <span className="user-data-title">
          <img
            src={BalanceIcon}
            alt="Balance icon"
            className="user-data-image"
          />
          ბალანსი
        </span>
        <span>995.65 GEL</span>
      </div>
      <div className="user-data -spins">
        <span className="user-data-title">
          <img src={SpinsIcon} alt="Spins icon" className="user-data-image" />
          უფასო სპინები
        </span>
        <span>18 GEL</span>
      </div>
    </div>
  );
};

export default UserBox;
