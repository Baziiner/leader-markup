import React, { useEffect } from 'react';
import Modal from 'react-modal';
import './CustomModal.scss';
import './GamePopup.scss';
import closeIcon from '../../assets/icons/close-ic.svg';

const CustomModal = ({ modalIsOpen, closeModal, className, children }) => {
  useEffect(() => {
    Modal.setAppElement('body');
  }, []);

  return (
    <Modal
      isOpen={modalIsOpen}
      onRequestClose={closeModal}
      className={`CustomModal ${className || ''}`}
    >
      <button onClick={closeModal} className="close-button">
        <img src={closeIcon} alt="close button" />
      </button>
      {children}
    </Modal>
  );
};

export default CustomModal;
