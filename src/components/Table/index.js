import React from 'react';
import './Table.scss';
import TableItem from './TableItem';

const Table = ({ className, headerChildren, itemChildren, data }) => {
  return (
    <div className="Table">
      <div className={`table-container ${className || ''}`}>
        <div className="table">
          <div className="table-header">
            <div className="table-header-wrapper">{headerChildren}</div>
          </div>
          <div className="table-main-content">
            <TableItem data={data} itemChildren={itemChildren} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Table;
