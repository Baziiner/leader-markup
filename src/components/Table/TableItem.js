import React from 'react';
import './Table.scss';

const TableItem = ({ data, itemChildren }) => {
  return (
    <>
      {data.map((data, index) => (
        <div className="table-item-container" key={index}>
          {itemChildren(data, index)}
        </div>
      ))}
    </>
  );
};

export default TableItem;
