import React from 'react';
import './LobbyContainer.scss';
import liveUserIcon from '../../assets/icons/active-user-ic.svg';
import notActiveUserIcon from '../../assets/icons/inactive-user-ic.svg';

const LobbyContainer = ({ data, handleRegister }) => {
  const maxUsers = data.type === 'set-5' ? 5 : 3;

  let users = [];
  for (let i = 0; i < maxUsers; ++i) {
    users.push(
      <img
        src={i <= data.connected - 1 ? liveUserIcon : notActiveUserIcon}
        alt={i <= data.connected - 1 ? 'active user' : 'not active'}
        className="available-users-icon"
        key={i}
      />
    );
  }

  return (
    <div className="LobbyContainer">
      <div className="LobbyContainer-content">
        <p className="lobby-type">
          {data.type === 'set-5' ? '5 კაციანი' : '3 კაციანი'}
        </p>
        <p className="lobby-win">
          {data.maxPull} <span className="lobby-currency"> GEL </span>
        </p>
        <p className="lobby-max">მაქს. მოგება</p>
        <div className="available-users"> {users} </div>
        <div className="entry-price">{data.entry} ლარი</div>
      </div>
      <button
        className="register-button"
        onClick={() =>
          handleRegister({
            registered: true,
            registeredData: {
              type: data.type,
              entry: data.entry,
              maxPull: data.maxPull,
              connected: data.connected + 1
            }
          })
        }
      >
        რეგისტრაცია
      </button>
    </div>
  );
};

export default LobbyContainer;
