import React from 'react';
import './JackpotCounter.scss';

const JackpotCounter = ({ data }) => {
  const dataValue = data.value.toString();
  const fullNum = dataValue.substr(0, dataValue.indexOf('.'));
  const decimal = dataValue.split('.').pop();

  return (
    <div className="JackpotCounter">
      <div className="jackpot-earning">
        {[...fullNum].map((value, index) => (
          <span className="earning-value" key={index}>
            {value}
          </span>
        ))}
        <div className="decimal-point" />
        {[...decimal].map((value, index) => (
          <span className="earning-value-decimal" key={index}>
            {value}
          </span>
        ))}
      </div>
      <div className="jackpot-title">{data.title}</div>
    </div>
  );
};

export default JackpotCounter;
