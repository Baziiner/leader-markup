import React, { useState } from 'react';
import './TopSpin.scss';
import liveUserIcon from '../../assets/icons/active-user-ic.svg';
import PlusIcon from '../../assets/icons/plus-ic.png';
import ArrowIcon from '../../assets/icons/arrow-ic.svg';

const TopSpin = ({ data, index }) => {
  const [showAll, setView] = useState(false);

  let initialUsers =
    data.userList.length > 3 ? data.userList.slice(0, 3) : data.userList;

  let viewUsers = !showAll ? initialUsers : data.userList;

  return (
    <div className="TopSpin" key={index}>
      <div className="spin-date">
        <span className="spin-time">23.04.19</span>
        <span className="spin-hour">20:33</span>
      </div>
      <p className="spin-win">
        {data.spinValue} <span className="spin-currency"> GEL</span>
      </p>
      <div className="user-list">
        <img src={liveUserIcon} alt="users" className="user-icon" />
        <div className={`available-users ${showAll ? '-active' : ''}`}>
          {viewUsers.map((el, index) =>
            index !== viewUsers.length - 1 ? (
              <span key={index}>{`${el}, `}</span>
            ) : (
              <span key={index}>{`${el}`}</span>
            )
          )}
        </div>
        <button onClick={() => setView(!showAll)} className="view-toggle">
          <img
            src={showAll ? ArrowIcon : PlusIcon}
            className={`toggle-image ${showAll ? '-rotate' : ''}`}
            alt="toggle icon"
          />
        </button>
      </div>
      <div className={`multiplier ${showAll ? '-toggle-active' : ''}`}>
        {data.mult}
      </div>
    </div>
  );
};

export default TopSpin;
