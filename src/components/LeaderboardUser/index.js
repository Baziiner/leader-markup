import React from 'react';
import './LeaderboardUser.scss';

const LeaderboardUser = ({ data, loggedUserId }) => {
  return (
    <div
      className={`LeaderboardUser ${
        data.id === loggedUserId.id ? '-active-user' : ''
      }`}
    >
      <div className="position">{data.pos}</div>
      <div className="user-details">
        <div className="username">{data.name}</div>
        <div className="score">{data.score}</div>
      </div>
      <div className="coin-details">
        <div className="coin-value">{data.coin}</div>
        <div className="coin-title">COIN</div>
      </div>
    </div>
  );
};

export default LeaderboardUser;
