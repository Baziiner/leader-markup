import React, { useState, useEffect } from 'react';
import './RegisteredLobby.scss';
import liveUserIcon from '../../assets/icons/active-user-large-ic.svg';
import notActiveUserIcon from '../../assets/icons/inactive-user-large-ic.svg';

const RegisteredLobby = ({ data, handleRegister, setModal }) => {
  useEffect(() => {
    const timer = setTimeout(() => {
      setSimulation({
        simulateFull: true,
        connectedUsers: data.type === 'set-5' ? 5 : 3
      });
    }, Math.floor(Math.random() * 10000));
    return () => clearTimeout(timer);
  }, [data.type]);

  const [simulatedLobby, setSimulation] = useState({
    simulateFull: false,
    connectedUsers: data.connected
  });
  const maxUsers = data.type === 'set-5' ? 5 : 3;
  let connectedUsers = simulatedLobby.connectedUsers;

  let users = [];
  for (let i = 0; i < maxUsers; ++i) {
    users.push(
      <img
        src={i <= connectedUsers - 1 ? liveUserIcon : notActiveUserIcon}
        alt={i <= connectedUsers - 1 ? 'active user' : 'not active'}
        className="available-users-icon"
        key={i}
      />
    );
  }
  return (
    <div className="RegisteredLobby">
      <div className="LobbyContainer-content">
        <div className="lobby-info">
          <p className="lobby-type">
            {data.type === 'set-5' ? '5 კაციანი' : '3 კაციანი'}
          </p>
          <div className="lobby-info-right">
            <div className="entry-price">{data.entry} ლარი</div>
            <p className="lobby-win">
              {data.maxPull} <span className="lobby-currency"> GEL </span>
            </p>
          </div>
        </div>
        {!simulatedLobby.simulateFull ? (
          <h4 className="registered-lobby-title">
            გთხოვთ დაელოდოთ მოწინააღდმეგეს
          </h4>
        ) : (
          <div className="lobby-game">
            <div className="game-content -coefficient">
              <p className="current-coefficient">5X</p>
              <p className="game-content-title">კოეფიციენტი</p>
            </div>
            <div className="game-content -type">
              <div className="game-image"></div>
              <p className="game-content-title">JAMING JARS</p>
            </div>
          </div>
        )}
        <div
          className={`available-users ${
            simulatedLobby.simulateFull ? 'ready-lobby' : ''
          }`}
        >
          {users}
        </div>
        {!simulatedLobby.simulateFull && (
          <div className="lobby-timer">
            <p className="timer-title">სავარაუდო მოლოდინის დრო: 30 წამი</p>
            <div className="lobby-animation">
              <div className="loader" />
            </div>
          </div>
        )}
        {!simulatedLobby.simulateFull ? (
          <button
            className="registered-button -unregister"
            onClick={() =>
              handleRegister({
                registered: false,
                registeredData: {}
              })
            }
          >
            გაუქმება
          </button>
        ) : (
          <button
            className="registered-button -start"
            onClick={() =>
              setModal({
                isOpen: true,
                modalContent: 'lobby-game',
                maxPull: data.maxPull,
                users: maxUsers
              })
            }
          >
            დაწყება
          </button>
        )}
      </div>
    </div>
  );
};

export default RegisteredLobby;
