import React, { useState } from 'react';
import JackpotCounter from '../../components/JackpotCounter';
import LobbyContainer from '../../components/LobbyContainer';
import UserBox from '../../components/UserBox';
import TopSpin from '../../components/TopSpin';
import Table from '../../components/Table';
import CustomModal from '../../components/CustomModal';
import RegisteredLobby from '../../components/RegisteredLobby';
import './Lobby.scss';
import MainIcon from '../../assets/icons/main-ic.svg';
import PurchaseIcon from '../../assets/icons/chips-purchase-ic.svg';
import SettingsIcon from '../../assets/icons/settings-ic.svg';
import liveUserIcon from '../../assets/icons/active-user-small-ic.svg';
import timeIcon from '../../assets/icons/history-ic.svg';
import { Link } from 'react-router-dom';
import ROUTES from '../routes';

const Lobby = () => {
  const [modal, setModal] = useState({
    isOpen: false,
    modalContent: ''
  });

  const [registeredLobby, handleRegister] = useState({
    registered: false,
    registeredData: {}
  });

  const handleModalClose = () => {
    setModal({
      isOpen: false,
      modalContent: ''
    });

    handleRegister({
      registered: false,
      registeredData: {}
    });
  };

  const [jackpotValues] = useState([
    {
      title: 'LEADER JACKPOT',
      value: 82591.92
    },
    {
      title: 'LEADER JACKPOT',
      value: 82591.92
    }
  ]);

  const [activeUsers] = useState('4364');

  const [activeLobby, setLobby] = useState('set-all');

  const [availableLobbies] = useState([
    {
      type: 'set-5',
      connected: 2,
      maxPull: 5000,
      entry: '5'
    },
    {
      type: 'set-5',
      connected: 2,
      maxPull: 5000,
      entry: '5'
    },
    {
      type: 'set-5',
      connected: 2,
      maxPull: 5000,
      entry: '5'
    },
    {
      type: 'set-5',
      connected: 2,
      maxPull: 5000,
      entry: '5'
    },
    {
      type: 'set-5',
      connected: 2,
      maxPull: 5000,
      entry: '5'
    },
    {
      type: 'set-5',
      connected: 2,
      maxPull: 5000,
      entry: '5'
    },
    {
      type: 'set-3',
      connected: 1,
      maxPull: 3000,
      entry: '5'
    },
    {
      type: 'set-3',
      connected: 1,
      maxPull: 3000,
      entry: '5'
    }
  ]);

  const [spinList] = useState([
    {
      spinValue: 5000,
      userList: ['ნოდარა', 'ელდარა', 'ემზარა', 'ზიარა', 'მერაბა'],
      mult: '1000x2'
    },
    {
      spinValue: 5000,
      userList: ['ნოდარა', 'ელდარა', 'ემზარა', 'ზიარა', 'მერაბა'],
      mult: '1000x2'
    },
    {
      spinValue: 5000,
      userList: ['ნოდარა', 'ელდარა', 'ემზარა', 'ზიარა', 'მერაბა'],
      mult: '1000x2'
    },
    {
      spinValue: 5000,
      userList: ['ნოდარა', 'ელდარა', 'ემზარა', 'ზიარა', 'მერაბა'],
      mult: '1000x2'
    },
    {
      spinValue: 5000,
      userList: ['ნოდარა', 'ელდარა', 'ემზარა', 'ზიარა', 'მერაბა'],
      mult: '1000x2'
    }
  ]);

  const [topList] = useState([
    {
      num: '# 4412452',
      time: '12:23',
      coeff: 2.37,
      user: 'Nodaradara',
      win: '33.35'
    },
    {
      num: '# 4412452',
      time: '12:23',
      coeff: 2.37,
      user: 'Nodaradara',
      win: '33.35'
    },
    {
      num: '# 4412452',
      time: '12:23',
      coeff: 2.37,
      user: 'Nodaradara',
      win: '33.35'
    },
    {
      num: '# 4412452',
      time: '12:23',
      coeff: 2.37,
      user: 'Nodaradara',
      win: '33.35'
    },
    {
      num: '# 4412452',
      time: '12:23',
      coeff: 2.37,
      user: 'Nodaradara',
      win: '33.35'
    },
    {
      num: '# 4412452',
      time: '12:23',
      coeff: 2.37,
      user: 'Nodaradara',
      win: '33.35'
    },
    {
      num: '# 4412452',
      time: '12:23',
      coeff: 2.37,
      user: 'Nodaradara',
      win: '33.35'
    },
    {
      num: '# 4412452',
      time: '12:23',
      coeff: 2.37,
      user: 'Nodaradara',
      win: '33.35'
    }
  ]);

  const [history] = useState([
    {
      date: '23 იანვ.',
      price: '2 ლარი',
      x: '100X',
      amount: '5 კაციანი',
      participants: ['ნოდარა', 'ელდარა', 'ემზარა', 'ზაირა', 'მერაბა'],
      position: 10,
      win: '5000 ლარი'
    },
    {
      date: '23 იანვ.',
      price: '2 ლარი',
      x: '100X',
      amount: '5 კაციანი',
      participants: ['ნოდარა', 'ელდარა', 'ემზარა', 'ზაირა', 'მერაბა'],
      position: 10,
      win: '5000 ლარი'
    },
    {
      date: '23 იანვ.',
      price: '2 ლარი',
      x: '100X',
      amount: '5 კაციანი',
      participants: ['ნოდარა', 'ელდარა', 'ემზარა', 'ზაირა', 'მერაბა'],
      position: 10,
      win: '5000 ლარი'
    },
    {
      date: '23 იანვ.',
      price: '2 ლარი',
      x: '100X',
      amount: '5 კაციანი',
      participants: ['ნოდარა', 'ელდარა', 'ემზარა', 'ზაირა', 'მერაბა'],
      position: 10,
      win: '5000 ლარი'
    },
    {
      date: '23 იანვ.',
      price: '2 ლარი',
      x: '100X',
      amount: '5 კაციანი',
      participants: ['ნოდარა', 'ელდარა', 'ემზარა', 'ზაირა', 'მერაბა'],
      position: 10,
      win: '5000 ლარი'
    },
    {
      date: '23 იანვ.',
      price: '2 ლარი',
      x: '100X',
      amount: '5 კაციანი',
      participants: ['ნოდარა', 'ელდარა', 'ემზარა', 'ზაირა', 'მერაბა'],
      position: 10,
      win: '5000 ლარი'
    },
    {
      date: '23 იანვ.',
      price: '2 ლარი',
      x: '100X',
      amount: '5 კაციანი',
      participants: ['ნოდარა', 'ელდარა', 'ემზარა', 'ზაირა', 'მერაბა'],
      position: 10,
      win: '5000 ლარი'
    },
    {
      date: '23 იანვ.',
      price: '2 ლარი',
      x: '100X',
      amount: '5 კაციანი',
      participants: ['ნოდარა', 'ელდარა', 'ემზარა', 'ზაირა', 'მერაბა'],
      position: 10,
      win: '5000 ლარი'
    }
  ]);

  const headerChildrenTop = (
    <>
      <div className="table-title -icon"></div>
      <div className="table-title -id">ნომერი</div>
      <div className="table-title -time">დრო</div>
      <div className="table-title -coeff">კოეფიციენტი</div>
      <div className="table-title -user">მომხმარებელი</div>
      <div className="table-title -win">მოგებული თანხა</div>
    </>
  );

  const itemChildrenTop = (data) => (
    <>
      <div className="table-item-child -icon">
        <img src={MainIcon} alt="leader icon" />
      </div>
      <div className="table-item-child -id">{data.num}</div>
      <div className="table-item-child -time">
        <img src={timeIcon} alt="user time" />
        {data.time}
      </div>
      <div className="table-item-child -coeff">
        <div className="coeff-data">{data.coeff}</div>
      </div>
      <div className="table-item-child -user">
        <img src={liveUserIcon} alt="user icon" />
        <span className="user-title">{data.user}</span>
      </div>
      <div className="table-item-child -win">
        <div className="win-data">{data.win}&#8382;</div>
      </div>
    </>
  );

  const gameStartPopup = (
    <div className="GamePopup">
      <div className="content-left">
        <h4 className="game-start">თამაში დაიწყო</h4>
        <div className="game-rules">
          <div className="rule game-duration">
            <span className="text-left">ხანგძლივობა</span>
            <span className="text-right">2 საათი</span>
          </div>
          <div className="rule coin-value">
            <span className="text-left">ქოინების რაოდენობა</span>
            <span className="text-right">2 ცალი</span>
          </div>
          <div className="rule min-bet">
            <span className="text-left">მინიმალური ფსონი</span>
            <span className="text-right">22 ლარი</span>
          </div>
          <div className="rule max-bet">
            <span className="text-left">მაქსიმალური ფსონი</span>
            <span className="text-right">78 ლარი</span>
          </div>
        </div>
        <p className="game-description">
          თუ რამე ტექსტი იქნება აგერ ააატექსტის ადგილი
        </p>
      </div>
      <div className="content-right">
        <div className="game-content -type">
          <div className="game-image"></div>
          <p className="game-content-title">JAMING JARS</p>
        </div>
        <p className="lobby-win">
          {modal.maxPull} <span className="lobby-currency"> GEL </span>
        </p>
        <div className="winner-rewards">
          {modal.users === 3 ? (
            <>
              <div className="reward">
                <span className="place">1 ადგილი</span>
                <span className="cash">
                  {modal.maxPull - modal.maxPull / 3} GEL
                </span>
              </div>
              <div className="reward">
                <span className="place">2 ადგილი</span>
                <span className="cash">{modal.maxPull / 3} GEL</span>
              </div>
            </>
          ) : (
            <>
              <div className="reward">
                <span className="place">1 ადგილი</span>
                <span className="cash">{modal.maxPull / 2} GEL</span>
              </div>
              <div className="reward">
                <span className="place">2 ადგილი</span>
                <span className="cash">
                  {modal.maxPull / 2 - modal.maxPull / 2 / 2.5} GEL
                </span>
              </div>
              <div className="reward">
                <span className="place">3 ადგილი</span>
                <span className="cash">{modal.maxPull / 2 / 2.5} GEL</span>
              </div>
            </>
          )}
        </div>
        <Link to={ROUTES.GAME} className="SelectButton">
          <button className="button-start">დაწყება</button>
        </Link>
      </div>
    </div>
  );

  const headerChildrenHistory = (
    <>
      <div className="table-title -history-date">თარიღი</div>
      <div className="table-title -price">ღირებულ.ი</div>
      <div className="table-title -coeff-history">რა Xი</div>
      <div className="table-title -amount">რაოდენობა</div>
      <div className="table-title -participants">მონაწილეები</div>
      <div className="table-title -position"> ადგილი </div>
      <div className="table-title -win-history">მოგებული</div>
    </>
  );

  const itemChildrenHistory = (data) => (
    <>
      <div className="table-item-child -history-date">{data.date}</div>
      <div className="table-item-child -price">{data.price}</div>
      <div className="table-item-child -coeff-history">{data.x}</div>
      <div className="table-item-child -amount">
        <div className="coeff-data">{data.amount}</div>
      </div>
      <div className="table-item-child -paricipants">
        <img src={liveUserIcon} alt="user icon" />
        <span className="user-title">
          {data.participants.map((item, index) => (
            <span key={index}>
              {index !== data.participants.length - 1 ? `${item}, ` : item}
            </span>
          ))}
        </span>
      </div>
      <div className="table-item-child -position">{data.position}</div>
      <div className="table-item-child -win-history">
        <div className="win-data">{data.win}</div>
      </div>
    </>
  );

  const historyModalContent = (
    <div className="history-content">
      <h4 className="history-title">ისტორია</h4>
      <Table
        data={history}
        headerChildren={headerChildrenHistory}
        itemChildren={itemChildrenHistory}
      />
    </div>
  );

  return (
    <div className="Lobby">
      <CustomModal
        modalIsOpen={modal.isOpen}
        ariaHideApp={false}
        children={
          modal.modalContent === 'history'
            ? historyModalContent
            : gameStartPopup
        }
        closeModal={() => handleModalClose()}
        className={
          modal.modalContent === 'history' ? 'table-modal' : 'game-modal'
        }
      />
      <div className="Lobby-container">
        {registeredLobby.registered && <div className="overlay" />}
        <div className="flexbox -container-header">
          <img src={MainIcon} alt="main-icon" />
          <div className="jackpot-counter-wrapper">
            {jackpotValues.map((data, index) => (
              <JackpotCounter data={data} key={index} />
            ))}
          </div>
          <div className="options-wrapper">
            <button className="purchase-button">
              <img src={PurchaseIcon} alt="purchase icon" />
              <span className="purchase-title"> Cashier </span>
            </button>
            <div className="settings-wrapper">
              <img src={SettingsIcon} alt="settings-icon" />
            </div>
          </div>
        </div>
        <div className="flexbox -margin-bottom">
          <div className="container-left">
            <div className="lobby-navigation">
              <div className="nav-buttons">
                <button
                  className={`lobby-select ${
                    activeLobby === 'set-all' ? '-active' : ''
                  }`}
                  onClick={() => setLobby('set-all')}
                >
                  ALL GAMES
                </button>
                <button
                  className={`lobby-select ${
                    activeLobby === 'set-3' ? '-active' : ''
                  }`}
                  onClick={() => setLobby('set-3')}
                >
                  ONLY 3 PLAYER
                </button>
                <button
                  className={`lobby-select ${
                    activeLobby === 'set-5' ? '-active' : ''
                  }`}
                  onClick={() => setLobby('set-5')}
                >
                  ONLY 5 PLAYER
                </button>
              </div>
              <div className="online-counter">
                <img src={liveUserIcon} alt="ative users" />
                <span className="live-users-title">
                  Online Players: {activeUsers}
                </span>
              </div>
            </div>
            <div className="active-lobbies">
              {registeredLobby.registered && (
                <RegisteredLobby
                  data={registeredLobby.registeredData}
                  handleRegister={handleRegister}
                  setModal={setModal}
                />
              )}
              {availableLobbies.map((data, index) => (
                <>
                  {(activeLobby === 'set-all' || data.type === activeLobby) && (
                    <LobbyContainer
                      data={data}
                      handleRegister={handleRegister}
                      key={index}
                    />
                  )}
                </>
              ))}
            </div>
            <div className="lobby-table">
              <div className="table-navigation">
                <button className="table-nav-item is-active">
                  ტოპ მოგებები
                </button>
                <button className="table-nav-item">ლიდერბორდი</button>
                <button className="table-nav-item">ისტორია</button>
                <button className="table-nav-item">წესები</button>
              </div>
              <Table
                headerChildren={headerChildrenTop}
                data={topList}
                itemChildren={itemChildrenTop}
              />
            </div>
          </div>
          <div className="container-right">
            <UserBox setModal={setModal} />
            <div className="top-spins-container">
              <p className="top-spins-title">ტოპ სპინები</p>
              {spinList.map((data, index) => (
                <TopSpin data={data} key={index} />
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Lobby;
