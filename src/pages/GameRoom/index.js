import React, { useState } from 'react';
import './GameRoom.scss';
import LeaderboardUser from '../../components/LeaderboardUser';
import MainIcon from '../../assets/icons/main-ic.svg';
import CupIcon from '../../assets/icons/winner-cup-ic.svg';
import { Link } from 'react-router-dom';
import ROUTES from '../routes';

const GameRoom = () => {
  const [loggedUserId] = useState({ id: 4 });
  const [users] = useState([
    {
      id: 1,
      pos: 1,
      name: 'Alberto',
      score: 322145,
      coin: 3264
    },
    {
      id: 2,
      pos: 2,
      name: 'Alberto',
      score: 322145,
      coin: 3264
    },
    {
      id: 3,
      pos: 3,
      name: 'Alberto',
      score: 322145,
      coin: 3264
    },
    {
      id: 4,
      pos: 4,
      name: 'Davitich',
      score: 322145,
      coin: 3264
    },
    {
      id: 5,
      pos: 5,
      name: 'Alberto',
      score: 322145,
      coin: 3264
    }
  ]);
  return (
    <div className="GameRoom">
      <div className="gameroom-sidebar">
        <div className="gameroom-win">
          <img src={MainIcon} alt="main icon" className="main-icon" />
          <p className="lobby-win">
            5000 <span className="lobby-currency">GEL </span>
          </p>
          <p className="price-pool">საპრიზო ფონდი</p>
        </div>
        <div className="gameroom-leaderboard">
          <div className="leaderboard-title">
            <img src={CupIcon} alt="cup icon" className="leaderboard-icon" />
            ლიდერბორდი
          </div>
          <div className="leaderboard">
            {users.map((data, index) => (
              <LeaderboardUser
                data={data}
                key={index}
                loggedUserId={loggedUserId}
              />
            ))}
          </div>
        </div>
      </div>
      <div className="gameroom-content">
        <div className="win-popup">
          <img src={CupIcon} alt="cup icon" className="leaderboard-icon" />
          <p className="victory">გილოცავთ!</p>
          <p className="victory-description">თქვენ დაიკავეთ პირველი ადგილი</p>
          <div className="win-amount-wrapper">
            <p className="lobby-win">
              5000 <span className="lobby-currency">GEL </span>
            </p>
            <p className="price-pool">მოგებული თანხა</p>
          </div>
          <Link to={ROUTES.HOME} className="SelectButton">
            <button className="close-button">დახურვა</button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default GameRoom;
